
package Tauler;

public class Tauler implements IColor{

    Casella[][] caselles = new Casella[8][8];
    
    
    public void crearTauler(){
            
            for (int i = 0; i < caselles.length; i++) {
                
                for (int j = 0; j < caselles[i].length; j++) {                  
                    
                    caselles[i][j]=new Casella(((i+j)%2==0)?
                            WHITE_BACKGROUND:RED_BACKGROUND);
                   
                    
                }
                
                
            }
            
        }
    
    public String toStringTauler(){
         StringBuilder strTauler = new StringBuilder();

    	for (int i = 0; i < caselles.length; i++) {
        	       	
            for (int j = 0; j < caselles[i].length; j++) {
                
                strTauler.append(caselles[i][j].getColor()).append("  ");
                              
            }
            
            strTauler.append("\n");
            	
         }

        return strTauler.toString();
        
    }
    
    public class Casella{
        
        String color;
        
        public Casella(String color){
            
            this.color = color;
            
        }

        public String getColor() {
            return color;
        }
        
        
        
        
    }
    
    
}
