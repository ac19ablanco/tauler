/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interficies_m03_practica;

/**
 *
 * @author elmar
 */
public interface IText extends IColor {
    
    default String modificarColorFont(String cod_color, String text){
        
        return cod_color+text+cod_color;
        
    }
    
    default String modificarColorFons(String cod_color, String text){
    
        return cod_color+text;
        
    }
    
    default String restablirConsola(){
        
        return ANSI_RESET;
        
    }
    
    
    abstract public String treureEspais(String text);
    
    abstract public String invertir(String text);
    
}
