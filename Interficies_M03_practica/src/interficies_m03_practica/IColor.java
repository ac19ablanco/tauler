/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interficies_m03_practica;

/**
 *
 * @author elmar
 */
public interface IColor {

    //COLOR LLETRES  
    String BLACK = "\u001B[30m";
    String RED = "\u001B[31m";
    String GREEN = "\u001B[32m";
    String YELLOW = "\u001B[33m";
    String BLUE = "\u001B[34m";
    String PURPLE = "\u001B[35m";
    String CYAN = "\u001B[36m";
    String WHITE = "\u001B[37m";

    //COLOR FONS LlETRES
    String BLACK_BACKGROUND = "\u001B[40m";
    String RED_BACKGROUND = "\u001B[41m";
    String GREEN_BACKGROUND = "\u001B[42m";
    String YELLOW_BACKGROUND = "\u001B[43m";
    String BLUE_BACKGROUND = "\u001B[44m";
    String PURPLE_BACKGROUND = "\u001B[45m";
    String CYAN_BACKGROUND = "\u001B[46m";
    String WHITE_BACKGROUND = "\u001B[47m";

    //restablir valors per defecte    
    String ANSI_RESET = "\u001B[0m";

}
