/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interficies_m03_practica;

/**
 *
 * @author elmar
 */
public class ImplementarInterficies {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        IText texto = new Text();
        String text1 = "HOLA MUNDO";
        
        System.out.println("Cambiar color text(HOLA MUNDO)--> "
                +texto.modificarColorFont(IColor.RED, text1)+
                texto.restablirConsola());
        System.out.println("Cambiar fondo texto(HOLA MUNDO)--> "
                +texto.modificarColorFons(IColor.RED_BACKGROUND,text1));
        System.out.println("Treure espais text(HOLA MUNDO)--> "
                +texto.treureEspais(text1));
        System.out.println("Invertir text(HOLA MUNDO)--> "
                +texto.invertir(text1));
        System.out.println("Contar caracteres text(HOLA MUNDO)--> "
                +Text.class.cast(texto).cmptDigits(text1));
        System.out.println("Contar caracteres sin espacios text(HOLA MUNDO)--> "
                +Text.class.cast(texto).cmptDigits(texto.treureEspais(text1)));        
        System.out.println(texto.restablirConsola()+"Fons Color Random"
                + " text(HOLA MUNDO)--> "+imprimirColorRandom(texto,text1));
    }
    
    static String imprimirColorRandom(IText itext, String text){
        
        int random = (int)(Math.random()*8);
        String colorRandom = "\u001B[4"+random+"m";
        
        return itext.modificarColorFons(colorRandom, text);
        
    }
}
