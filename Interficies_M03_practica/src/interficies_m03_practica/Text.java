/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interficies_m03_practica;

/**
 *
 * @author elmar
 */
public class Text implements IText{

    @Override
    public String treureEspais(String text) {
        return text.replaceAll(" ", "");
    }

    @Override
    public String invertir(String text) {
        StringBuilder stringBuilder = new StringBuilder(text);
        return stringBuilder.reverse().toString();
    }
    
    public int cmptDigits(String text){
        return text.length();
    }
    
}
